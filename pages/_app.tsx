/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from "react";
import App from "next/app";
import { useRouter } from "next/router";
import "../styles/globals.css";
import "../styles/paylineDetails.css";
import client from "lib/apolloClient";
import { ApolloProvider, gql } from "@apollo/client";
import Layout from "@/components/UI/layouts/Layout";
import BusinessCategoryProvider from "../context/BusinessCategoryProvider";

interface AppProps {
  children: React.ReactNode;
  Component: React.ComponentClass;
  pageProps: Record<string, unknown>;
  navBar: any;
}

const Payline = ({ Component, pageProps, navBar }: AppProps) => {
  const router = useRouter();
  return (
    <BusinessCategoryProvider slug={router.query.slug}>
      <ApolloProvider client={client}>
        <Layout navBar={navBar}>
          <Component {...pageProps} />
        </Layout>
      </ApolloProvider>
    </BusinessCategoryProvider>
  );
};

const navBarQuery = gql`
  query navBarQuery {
    websiteCopy {
      navbar {
        navbar {
          navItem
          navLink
          navChild {
            icon {
              sourceUrl
              altText
            }
            link
            linkName
          }
        }
      }
    }
  }
`;

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.

Payline.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);

  const { data } = await client.query({ query: navBarQuery });

  const navBar = data.websiteCopy.navbar.navbar;
  return { ...appProps, navBar };
};

export default Payline;
