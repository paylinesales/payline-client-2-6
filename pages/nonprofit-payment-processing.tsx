import Head from "next/head";
import React from "react";
import { BsPhone } from "react-icons/bs";
import { FiRefreshCcw } from "react-icons/fi";
import { MdOutlineAttachMoney } from "react-icons/md";
import { AiOutlineUnlock } from "react-icons/ai";
import LetsGetInTouchSection from "@/components/UI/templates/sections/home/LetsGetInTouch/LetsGetInTouchSection";
import BlogSection from "@/components/UI/templates/sections/home/BlogSection/BlogSection";

const pageData = {
  hero: {
    text: "Payment Processing Solutions",
    title: "Nonprofit & Educational",
  },
};

const logoBar = [
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/occasion.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/ace.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/school.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/forever.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/boonli.png",
  "https://paylinelocal.wpengine.com/wp-content/uploads/2021/09/payzer.png",
];

const NonProfit = () => {
  return (
    <>
      <Head>
        <title>Nonprofit Payment Processing | Payline</title>
      </Head>
      {/** Main Hero * */}
      <section className="md:flex relative py-5 sm:py-12" id="main-hero">
        <img
          src="https://via.placeholder.com/1441x561"
          className="absolute top-16 xs:top-0 left-0 w-full h-full -z-10 object-cover object-right-77 xs:object-right-70 md:object-bottom"
        />

        <div className="container mx-auto my-8 sm:my-12 py-12 flex items-center">
          <div className="grid grid-cols-6 pb-20 sm:pb-0 my-12 ml-20 xs:mx-9 h-fit-content flex-1">
            <div className="col-span-6">
              <h1 className="text-2xl xs:text-3xl md:text-5xl xl:text-6xl font-hkgrotesk font-bold text-payline-black -mt-20 xs:-mt-0">
                <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
                  {pageData.hero.title}
                </span>
                <br />
                {pageData.hero.text}
              </h1>
            </div>
          </div>
        </div>
      </section>

      <div className="sm:ml-auto sm:mr-0 lg:mr-8 lg:-mt-16 xl:-mt-10 sm:w-6/12 sm:mb-5 lg:mb-8">
        <div className="grid grid-cols-2 place-items-center xs:justify-evenly xs:flex xs:flex-row xs:flex-wrap filter grayscale items-center bg-payline-white rounded p-6 drop-shadow-none my-6 lg:drop-shadow-md xs:gap-x-2 gap-y-6">
          {logoBar?.map((logo) => {
            const sourceUrl = logo;
            return <img className="max-w-6rem" src={sourceUrl} />;
          })}
        </div>
      </div>

      <section className="w-full md:w-6/12 px-4 my-20 text-center mx-auto">
        <div className="flex flex-wrap">
          <div className="w-full mb-10">
            <h3 className="text-lg text-payline-black font-bold text-3xl ">
              All the Features You Need To Change the World
            </h3>
          </div>
          <div className="w-full md:w-6/12 px-4">
            <div className="relative flex flex-col mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <BsPhone className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  ACCEPT CHIPS & MOBILE PAYMENTS
                </h6>
                <p className="mb-4 text-color">
                  Your business won’t skip beat with EMV/Chip & mobile payment
                  services. Give your patients what they want.
                </p>
              </div>
            </div>
            <div className="relative flex flex-col min-w-0">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <MdOutlineAttachMoney className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  SIMPLE TRANSPARENT PRICING
                </h6>
                <p className="mb-4 text-color">
                  This extension also comes with 3 sample pages. They are fully
                  coded so you can start working instantly.
                </p>
              </div>
            </div>
          </div>
          <div className="w-full md:w-6/12 px-4">
            <div className="relative flex flex-col min-w-0 mt-4">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <FiRefreshCcw className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black">
                  SEAMLESS INTEGRATION
                </h6>
                <p className="mb-4 text-color">
                  We also feature many dynamic components for React, NextJS, Vue
                  and Angular.
                </p>
              </div>
            </div>
            <div className="relative flex flex-col min-w-0">
              <div className="px-4 py-5 flex-auto">
                <div className="text-blueGray-500 text-center inline-flex items-center justify-center w-12 h-12 mb-5">
                  <AiOutlineUnlock className="w-full h-full color-peach" />
                </div>
                <h6 className="text-xl mb-1 font-semibold text-payline-black ">
                  RECURRING AND SCHEDULED BILLING
                </h6>
                <p className="mb-4 text-color">
                  Store and tokenize your customer payment information with our
                  secure gateway. Allow for easy checkout, refunds, and sending
                  receipts along with scheduled and recurring billing.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h2 className="mb-8 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              YOUR SUITE OF TOOLS
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Invoicing and a Payments Page
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Our secure and compliant software encrypts and tokenizes user and
              card data which allows you to send invoices, set up recurring
              billing, or schedule payments until complete. We also provide you
              with a hosted payments page if you have a less demanding product
              line or are a 5013c. Surcharge and convenience fees are available.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/invoice-dashboard-payline-e1581981558255.jpg"
            />
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2  items-start">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/clover-go-mobile-online-offline-app.jpg"
            />
          </div>
          <div className="flex flex-col mb-16 text-left lg:flex-grow md:w-1/2 md:mb-0  lg:pl-24 md:pl-16">
            <h2 className="mb-8 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              PAYMENTS ON THE GO
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Mobile Readers & Apps
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Use your iPhone or Android to take payments inside your store or
              at your customer’s location. Multiple solutions ready to go
              out-of-the box by downloading a mobile payment app and connecting
              to your phone or wireless bluetooth options. Upload your products,
              email receipts, auto-calculate taxes, surcharge, void, refund, and
              more. Swipe, Chip, Tap.
            </p>
          </div>
        </div>
      </section>

      <section className="text-blueGray-700 bg-white mt-20">
        <div className="container flex flex-col items-center px-5 py-16 mx-auto md:flex-row">
          <div className="flex flex-col items-start mb-16 text-left lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 md:mb-0">
            <h2 className="mb-8 text-xs font-semibold tracking-widest text-black uppercase title-font payline-subhead">
              INTEGRATIONS
            </h2>
            <h1 className="mb-8 text-2xl font-extrabold font-inter tracking-tighter text-black md:text-5xl title-font">
              Work Within Your Existing Software or Shopping Cart
            </h1>
            <p className="mb-8 text-base leading-relaxed text-left text-color">
              Lower your price while integrating into your existing software
              platform or shopping cart. 200+ integrations and a dedicated
              Solutions Engineer to get you from start to finish in the same
              day.
            </p>
          </div>
          <div className="w-full lg:w-1/3 lg:max-w-lg md:w-1/2">
            <img
              className="object-cover object-center rounded-lg "
              alt="hero"
              src="https://paylinelocal.wpengine.com/wp-content/uploads/2021/10/ecommerce.jpg"
            />
          </div>
        </div>
      </section>

      <BlogSection />
      <LetsGetInTouchSection />
    </>
  );
};

export default NonProfit;
