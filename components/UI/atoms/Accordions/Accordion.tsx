/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from "react";
import { XIcon } from "@heroicons/react/solid";
import AccordionItem from "./AccordionItem";

interface AccordionProps {
  title: React.ReactNode | string;
  className?: string;
  content: React.ReactNodeArray;
}

export const Accordion: React.FC<AccordionProps> = ({
  title,
  content,
  className,
}) => {
  const [active, setActive] = useState(false);

  const isActive = active;

  const toggleAccordion = () => {
    setActive((prev) => !prev);
  };

  return (
    <div className={`mx-4 flex flex-col rounded ${className}`}>
      <button
        type="button"
        onClick={toggleAccordion}
        className="py-4 rounded bg-payline-background-light border-payline-border-light  box-border appearance-none cursor-pointer focus:outline-none flex items-center justify-between flex-grow">
        <p className="pl-8">{title}</p>
        <XIcon
          className={`pr-4 transform duration-700 ease ${
            isActive ? " " : "rotate-45"
          }`}
          height="20px"
        />
      </button>
      <div
        id="ease"
        className={`transition duration-700 ease-in-out ${
          isActive ? "h-auto" : "h-0"
        }  bg-payline-background-white md:bg-payline-white overflow-hidden transition-max-height  duration-300 ease-in-out divide-y`}>
        <AccordionItem content={content} />
      </div>
    </div>
  );
};

Accordion.defaultProps = {
  className: "",
};
