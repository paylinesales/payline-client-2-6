import React from "react";
import Link from "next/link";
import PaylineDetail from "@/components/UI/atoms/PaylineDetail";

type categoryLink = {
  name: string;
  href: string;
};

type categoryProps = {
  title: string;
  links: Array<categoryLink>;
};

const FooterCategory: React.FC<categoryProps> = ({ title, links }) => {
  return (
    <div className="flex flex-col lg:">
      {/* Desktop layout */}
      <div className="hidden lg:block">
        <div className="w-full pb-4 text-lg capitalize font-bold font-opensans text-payline-black">
          {title}
        </div>
        {links.map(({ name, href }) => {
          return (
            <div
              key={name}
              className="w-full py-1 capitalize font-opensans text-payline-black hover:underline">
              <Link href={href}>{name}</Link>
            </div>
          );
        })}
      </div>
      {/* Mobile layout */}
      <div className="block lg:hidden">
        <PaylineDetail
          title={title}
          className="text-lg capitalize font-bold font-opensans text-payline-black transition-colors duration-150"
          summaryClasses="font-semibold flex pl-0 text-2xl">
          {links.map(({ name, href }) => {
            return (
              <div className="w-full py-1 capitalize font-opensans text-payline-black hover:underline font-normal">
                <Link href={href}>{name}</Link>
              </div>
            );
          })}
        </PaylineDetail>
      </div>
    </div>
  );
};

export default FooterCategory;
