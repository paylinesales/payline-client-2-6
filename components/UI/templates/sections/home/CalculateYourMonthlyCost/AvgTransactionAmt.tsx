import React from "react";

import PaylineButton from "@/components/UI/atoms/PaylineButton";

const hasPlusSign = (arrLength: number, i: number) => {
  return arrLength === i + 1 ? "+" : "";
};

const isDefault = (activeValue: number, amount: number) => {
  return activeValue === amount
    ? "bg-payline-dark text-payline-white"
    : "bg-payline-background-light text-payline-dark";
};

const AvgTransactionAmt: React.FC<{
  amounts: number[];
  onChange: (event: React.MouseEvent<HTMLButtonElement>) => void;
  activeValue: number;
}> = (props) => {
  const { amounts, onChange: updateAvgTransactionAmtVal, activeValue } = props;
  return (
    <div className="my-6 flex flex-wrap gap-2">
      {amounts.map((amount, i) => {
        return (
          <PaylineButton
            className={`flex-grow lg:flex-none px-3 focus:bg-payline-dark focus:text-white lg:text-xs xl:text-base w-5/12 md:w-auto ${isDefault(
              activeValue,
              amount,
            )}`}
            disabled={false}
            onClick={updateAvgTransactionAmtVal}
            key={amount}
            value={amount}>
            {`$${amount}${hasPlusSign(amounts.length, i)}`}
          </PaylineButton>
        );
      })}
    </div>
  );
};

export default AvgTransactionAmt;
