/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React, { useEffect, useState } from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import { useRouter } from "next/dist/client/router";
import PaymentOptionsBanner from "@/components/UI/organisms/banners/PaymentOptionsBanner";
import ColorTextBox from "../../../atoms/ColorTextBox";
import HalfCircle from "../../../atoms/Icons/HalfCircle";
import { PAYMENT_TYPES, BUSINESS_TYPES } from "../../../../../constants";
import { Page_Revenuesection as revenueSectionType } from "../../../../../generated/apolloComponents";

const { SMALL_BUSINESS } = BUSINESS_TYPES;

const { ONLINE, IN_PERSON } = PAYMENT_TYPES;

interface Props {
  revenueSection: revenueSectionType;
}

const RevenueSection = ({ revenueSection }: Props): React.ReactElement => {
  let sourceUrl = "";
  const { sectionImage, bulletPoints, sectionText, paymentOptionsLabel } =
    revenueSection!;
  sourceUrl = sectionImage?.sourceUrl;
  const { sectionHeadline, subheadline } = sectionText!;
  const { highlightedWord, headlineRemainder } = sectionHeadline!;
  const [businessTypeState, setBusinessTypeState] = useState(IN_PERSON);
  const { paymentType, businessType, slug } = useBusinessCategory();
  const router = useRouter();

  const isOnlineBusiness = paymentType === ONLINE;
  const isSmallBusiness = businessType === SMALL_BUSINESS;
  const businessSlug = isSmallBusiness ? "/small-business" : "/enterprise";
  useEffect(() => {
    if (isOnlineBusiness) {
      setBusinessTypeState(ONLINE);
    } else {
      setBusinessTypeState(IN_PERSON);
    }
  }, [isOnlineBusiness]);
  const businessTypeHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    const activeBusinessType = event.currentTarget.dataset.type;
    setBusinessTypeState(activeBusinessType!);
    if (slug !== undefined) {
      router.push(
        `${businessSlug}/${activeBusinessType}`,
        `${businessSlug}/${activeBusinessType}`,
        {
          scroll: false,
        },
      );
    }
  };

  return (
    <section
      className="container mx-auto relative px-9 lg:px-0 mb-24"
      id="revenue-section">
      <PaymentOptionsBanner
        className="lg:absolute -top-40 px-0 lg:px-8 xl:w-2/5 w-full md:w-1/2 md:mx-auto"
        businessType={businessTypeState}
        businessTypeHandler={businessTypeHandler}>
        <div
          id="payment_options--top"
          className="flex gap-3 items-center font-bold mb-4 justify-center lg:justify-start">
          <HalfCircle className="hidden lg:block" />
          <p className="font-bold text-payline-black">{paymentOptionsLabel}</p>
        </div>
      </PaymentOptionsBanner>
      <div className="flex justify-center xl:justify-between flex-wrap lg:flex-nowrap items-center lg:mx-16 mt-16  gap-8">
        <img src={sourceUrl} className="mx-auto xl:w-4/12 md:w-5/12 w-full" />
        <div className="mb-10">
          <h2 className="text-4xl font-hkgrotesk leading-tight font-bold text-payline-black mb-5">
            <ColorTextBox>{highlightedWord}</ColorTextBox>
            <span className="md:hidden">
              <br />
            </span>
            {headlineRemainder}
          </h2>
          <p className="mb-8 text-lg font-opensans text-payline-black">
            {subheadline}
          </p>
          {bulletPoints!.map((bulletPoint) => {
            const { bulletText } = bulletPoint!;
            return (
              <h4 className="mb-8 text-4xl leading-tight font-bold text-payline-disabled">
                {bulletText}
              </h4>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default RevenueSection;
