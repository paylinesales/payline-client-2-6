import { gql } from "@apollo/client";

const signUpQuery = gql`
  query signUpQuery {
    pages(where: { title: "SignUp" }) {
      edges {
        node {
          seo {
            canonical
            cornerstone
            focuskw
            breadcrumbs {
              text
              url
            }
            fullHead
            metaDesc
            metaKeywords
            metaRobotsNofollow
            metaRobotsNoindex
            opengraphAuthor
            opengraphDescription
            opengraphImage {
              altText
              ancestors {
                edges {
                  node {
                    id
                  }
                }
              }
              author {
                node {
                  description
                  email
                  firstName
                  seo {
                    fullHead
                    metaDesc
                    metaRobotsNofollow
                    metaRobotsNoindex
                    title
                    schema {
                      raw
                    }
                  }
                  slug
                  uri
                  url
                  userId
                  username
                }
              }
              authorDatabaseId
              authorId
              caption
              commentCount
              commentStatus
              date
              dateGmt
              description
              desiredSlug
              fileSize
              enclosure
            }
            twitterTitle
            twitterImage {
              sizes
              slug
              sourceUrl
              srcSet
              status
              template {
                templateName
              }
              title
              uri
              description
              altText
              ancestors {
                edges {
                  node {
                    id
                  }
                }
              }
              authorDatabaseId
              authorId
              caption
              enclosure
              mediaItemId
              mediaItemUrl
              mediaType
              mimeType
              modified
              modifiedGmt
              parentDatabaseId
              parentId
              previewRevisionDatabaseId
              previewRevisionId
              seo {
                twitterTitle
                title
                twitterDescription
                schema {
                  raw
                }
                canonical
                cornerstone
                breadcrumbs {
                  text
                }
                focuskw
                fullHead
                metaDesc
                metaKeywords
                metaRobotsNofollow
                metaRobotsNoindex
                opengraphAuthor
                opengraphDescription
                opengraphImage {
                  sourceUrl
                }
              }
            }
          }
          signUpPage {
            backgroundImage {
              altText
              sourceUrl
              title
            }
            foregroundImage {
              title
              sourceUrl
              altText
            }
            headline {
              part1
              part2
              part3
              part4
              part5
            }
            hubspotForm {
              formId
              portalid
              region
            }
            subtleCta
          }
          signUpStep2 {
            signUpItem {
              link {
                url
                title
              }
              label
              image {
                sourceUrl
                altText
              }
            }
          }
          slug
          title
          status
          authorId
          date
          id
        }
      }
    }
  }
`;

export default signUpQuery;
