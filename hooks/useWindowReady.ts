import { useEffect, useState } from "react";

const useWindowReady = () => {
  const [ready, setIsReady] = useState(false);

  useEffect(() => {
    setIsReady(true);
  }, []);

  return ready;
};

export default useWindowReady;
